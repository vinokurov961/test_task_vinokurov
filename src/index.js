import './assets/styles/app.scss'
import obiImg from "./assets/images/mainPage/obi.png";
import leruaImg from "./assets/images/mainPage/lerua.png";
import klumbaImg from "./assets/images/mainPage/klumba.png";
import flowerImg from "./assets/images/mainPage/flower.png";
import OrchidImg from "./assets/images/mainPage/Orchid.png";
import PionsImg from "./assets/images/mainPage/Pions.png";
import AloeImg from "./assets/images/mainPage/Aloe.png";
import ChrysanthemumImg from "./assets/images/mainPage/Chrysanthemum.png";
import postImg from "./assets/images/mainPage/post.png";
import footerIcon from "./assets/images/mainPage/footerIcon.png";
import potsplantsImg from "./assets/images/mainPage/potsplants.png";
import solidsoImg from "./assets/images/mainPage/solidso.png";

const buttonNavMenu = document.querySelector(".menu-utils__button-burger");
const navMenu = document.querySelector("#vanMenu");
buttonNavMenu.addEventListener('click', () => {
  if (navMenu.classList.contains("active")) document.querySelector("#vanMenu").classList.remove("active")
  else document.querySelector("#vanMenu").classList.add("active");
})
